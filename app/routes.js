const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({ 'data': {} });
    });

    app.get('/rates', (req, res) => {
        return res.send({
            rates: exchangeRates
        });
    });

    app.post('/currency', (req, res) => {
        const { name, ex, alias } = req.body;

        if (name && ex && alias) {
            return res.status(200).send({
                currency: req.body
            });
        }

        if (!name) {
            return res.status(400).send({
                'error': "Bad Request - missing required parameter NAME"
            });
        }

        if (!ex) {
            return res.status(400).send({
                'error': "Bad Request - missing required parameter EX"
            });
        }

        if (!alias) {
            return res.status(400).send({
                'error': "Bad Request - missing required parameter ALIAS"
            });
        }

        if (typeof name !== 'string') {
            return res.status(400).send({
                'error': "Bad Request - parameter name MUST be a STRING"
            });
        }

        if (name.length === 0) {
            return res.status(400).send({
                'error': "Bad Request - parameter name MUST NOT be an EMPTY STRING"
            });
        }

        if (typeof alias !== 'string') {
            return res.status(400).send({
                'error': "Bad Request - parameter alias MUST be a STRING"
            });
        }

        if (alias.length === 0) {
            return res.status(400).send({
                'error': "Bad Request - parameter alias MUST NOT be an EMPTY STRING"
            });
        }

        if (typeof ex !== 'object' || Object.keys(ex).length === 0) {
            return res.status(400).send({
                'error': "Bad Request - parameter EX must be a non-empty object"
            });
        }

       	if (exchangeRates[name.toLowerCase()]) {
        	return res.status(400).send({
            	'error': "Bad Request - currency with this alias already exists"
        	});
    	}

    	if (Object.values(exchangeRates).find(currency => currency.alias.toLowerCase() === alias.toLowerCase())) {
            return res.status(400).send({
                'error': "Bad Request - currency with this alias already exists"
            });
        }
    });
};