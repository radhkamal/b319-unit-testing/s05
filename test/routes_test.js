const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5295').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5295')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5295')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})

    it('test_api_post_currency_is_200', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': 'Japan',
            	'name': 'Japanese Yen',
		      	'ex': {
			        'peso': 0.47,
			        'usd': 0.0092,
			        'won': 10.93,
			        'yuan': 0.065
		      	}
            })
            .end((err, res) => {
                expect(res.status).to.equal(200);
                done();
          });
	})

	it('test_api_post_currency_returns_400_if_no_currency_name', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': 'Japan',
		      	'ex': {
			        'peso': 0.47,
			        'usd': 0.0092,
			        'won': 10.93,
			        'yuan': 0.065
		      	}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
          });
	})

	it('test_api_post_currency_returns_400_if_currency_name_is_not_string', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': 'Japan',
            	'name': 99999,
		      	'ex': {
			        'peso': 0.47,
			        'usd': 0.0092,
			        'won': 10.93,
			        'yuan': 0.065
		      	}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
          });
	})

	it('test_api_post_currency_returns_400_if_currency_name_is_empty_string', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': 'Japan',
            	'name': '',
		      	'ex': {
			        'peso': 0.47,
			        'usd': 0.0092,
			        'won': 10.93,
			        'yuan': 0.065
		      	}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
	})

	it('test_api_post_currency_returns_400_if_no_currency_ex', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': 'Korea',
            	'name': 'South Korean Won'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
	})

	it('test_api_post_currency_returns_400_if_currency_ex_not_object', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': 'Korea',
            	'name': 'South Korean Won',
            	'ex': 99999 
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
	})

	it('test_api_post_currency_returns_400_if_no_ex_content', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': 'Korea',
            	'name': 'South Korean Won',
            	'ex': {} 
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
	})

	it('test_api_post_currency_returns_400_if_no_currency_alias', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'name': 'Euro',
		      	'ex': {
			        'peso': 0.47,
			        'usd': 0.0092,
			        'won': 10.93,
			        'yuan': 0.065
		      	}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
	})

	it('test_api_post_currency_returns_400_if_currency_alias_is_empty_string', (done) => {
        chai.request('http://localhost:5295')
            .post('/currency')
            .send({
            	'alias': '',
            	'name': 'Korean South Korean Won',
		      	'ex': {
			        'peso': 0.47,
			        'usd': 0.0092,
			        'won': 10.93,
			        'yuan': 0.065
		      	}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
	})

	it('test_api_post_currency_returns_400_if_duplicate_alias_is_found', (done) => {
    	chai.request('http://localhost:5295')
	        .post('/currency')
	        .send({
	            'name': 'usd',
	            'ex': {
	                'peso': 0.47,
	                'usd': 0.0092,
	                'won': 10.93,
	                'yuan': 0.065
	            }
	        })
	        .end((err, res) => {
	            expect(res.status).to.equal(400);
	            done();
	    });
	})


	it('test_api_post_currency_is_200_if_complete_input_is_given', (done) => {
	    chai.request('http://localhost:5295')
	        .post('/currency')
	        .send({
	            'alias': 'Japan',
	            'name': 'Japanese Yen',
	            'ex': {
	                'peso': 0.47,
	                'usd': 0.0092,
	                'won': 10.93,
	                'yuan': 0.065
	            }
	        })
	        .end((err, res) => {
	            expect(res.status).to.equal(200);
	            done();
	        });
	});

	it('test_api_post_currency_returns_200_if_complete_input_no_duplicates', (done) => {
	    chai.request('http://localhost:5295')
	        .post('/currency')
	        .send({
	            'alias': 'Euro',
	            'name': 'Euro',
	            'ex': {
	                'usd': 1.18,
	                'gbp': 0.85,
	                'jpy': 129.52
	            }
	        })
	        .end((err, res) => {
	            expect(res.status).to.equal(200);
	            done();
	        });
	});

})
